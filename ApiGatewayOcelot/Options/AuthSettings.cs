namespace ApiGatewayOcelot.Options
{
    public class AuthSettings
    {
        public string Host { get; set; }
        public string Action { get; set; }
    }
}
using ApiGatewayOcelot.Middlewares.ExceptionMiddlewares;
using ApiGatewayOcelot.Options;
using CacheManager.Core;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Ocelot.Cache.CacheManager;
using Ocelot.DependencyInjection;
using Ocelot.Middleware;
using Swashbuckle.AspNetCore.SwaggerUI;

namespace ApiGatewayOcelot
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddOcelot(Configuration)
                .AddCacheManager(x =>
                {
                    //write response on redis
                    x.WithRedisConfiguration(Configuration.GetValue<string>("CacheSettings:InstanceName"),
                            config =>
                            {
                                config.WithAllowAdmin()
                                    .WithDatabase(0)
                                    .WithEndpoint(Configuration.GetValue<string>("CacheSettings:Host"),
                                        6379); //Exs: Host: 127.0.0.1, Port: 6379
                            })
                        .WithJsonSerializer()
                        .WithRedisCacheHandle(Configuration.GetValue<string>("CacheSettings:InstanceName"));
                })
                //Check user token validate
                .AddDelegatingHandler<AuthHandler>();

            services.Configure<AuthSettings>(Configuration.GetSection("AuthSettings"));

            services.AddSwaggerForOcelot(Configuration,
                (o) => { o.GenerateDocsForGatewayItSelf = true; });
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo {Title = "ApiGateway-v1", Version = "v1"});
            });
        }

        public async void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

                app.UseSwagger();
                app.UseSwaggerForOcelotUI(opt =>
                {
                    opt.PathToSwaggerGenerator = "/swagger/docs";
                    opt.DocExpansion(DocExpansion.None);
                });
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseMiddleware<ExceptionMiddleware>();
            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
            await app.UseOcelot();
        }
    }
}
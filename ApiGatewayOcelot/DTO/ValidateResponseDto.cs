using System.Collections.Generic;

namespace ApiGatewayOcelot.DTO
{
    public class ValidateResponseDto
    {
        public int UserId { get; set; }
        public IReadOnlyList<string> Permissions { get; set; }
    }
}
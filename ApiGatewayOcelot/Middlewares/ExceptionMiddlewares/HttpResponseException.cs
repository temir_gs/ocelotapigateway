using System;

namespace ApiGatewayOcelot.Middlewares.ExceptionMiddlewares
{
    public class HttpResponseException : Exception
    {
        public virtual int? StatusCode { get; set; }
        public virtual string Code { get; set; }
        public virtual new string Message { get; set; }
    }
}
using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Serilog;

namespace ApiGatewayOcelot.Middlewares.ExceptionMiddlewares
{
     public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IHostEnvironment _hostEnvironment;

        public ExceptionMiddleware(RequestDelegate next, IHostEnvironment hostEnvironment)
        {
            _next = next;
            _hostEnvironment = hostEnvironment;
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (Exception exception)
            {
                await HandleExceptionAsync(httpContext, exception);
            }
        }

        private Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            HttpResponseException httpException = exception as HttpResponseException;
            context.Response.ContentType = "application/json";

            if (httpException == null)
            {
                context.Response.StatusCode = StatusCodes.Status500InternalServerError;
                //Logging
                WriteLog(exception, StatusCodes.Status500InternalServerError, context.Request.Path);
                return context.Response.WriteAsync(JsonConvert.SerializeObject(new
                {
                    Code = "UNKNOWN_EXCEPTION",
                    exception.Message
                }, Formatting.Indented));
            }

            context.Response.StatusCode = httpException.StatusCode ?? StatusCodes.Status500InternalServerError;
            if (_hostEnvironment.IsDevelopment())
            {
                return context.Response.WriteAsync(JsonConvert.SerializeObject(new
                {
                    httpException.Code,
                    httpException.StatusCode,
                    httpException.Message,
                    exception.StackTrace
                }, Formatting.Indented));
            }

            //Logging
            WriteLog(exception, httpException.StatusCode ?? StatusCodes.Status500InternalServerError,
                context.Request.Path);

            return context.Response.WriteAsync(JsonConvert.SerializeObject(new
            {
                httpException.Code,
                httpException.StatusCode,
                httpException.Message
            }, Formatting.Indented));
        }

        private static void WriteLog(Exception exception, int statusCode, string requestPath)
        {
            string logData = JsonConvert.SerializeObject(new
            {
                requestPath,
                statusCode,
                exception.Message,
                exception.StackTrace
            }, Formatting.Indented);
            Log.Error($"Something went wrong: {logData}");
        }
    }
}
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using ApiGatewayOcelot.DTO;
using ApiGatewayOcelot.Middlewares.ExceptionMiddlewares;
using ApiGatewayOcelot.Options;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace ApiGatewayOcelot
{
    public class AuthHandler : DelegatingHandler
    {
        private readonly AuthSettings authSettings;

        public AuthHandler(IOptions<AuthSettings> authSettings)
        {
            this.authSettings = authSettings.Value;
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request,
            CancellationToken cancellationToken)
        {
            using HttpClient client = new HttpClient();

            var actionPermissions = request.Headers.GetValues("Role");

            client.DefaultRequestHeaders.Authorization = request.Headers.Authorization;

            //Connect Auth microservice
            HttpResponseMessage response = await client.GetAsync(
                $@"{authSettings.Host}/{authSettings.Action}", cancellationToken);

            if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                throw new HttpResponseException()
                    {Code = "UNAUTHORIZED", StatusCode = 401, Message = "Unauthorized."};
            }

            ValidateResponseDto validateResponse =
                JsonConvert.DeserializeObject<ValidateResponseDto>(
                    await response.Content.ReadAsStringAsync(cancellationToken));

            if (validateResponse.Permissions == null || validateResponse.UserId <= 0)
            {
                throw new HttpResponseException()
                    {Code = "UNAUTHORIZED", StatusCode = 401, Message = "Unauthorized."};
            }

            foreach (var userPermission in validateResponse.Permissions)
            {
                if (actionPermissions.Any(p => p == userPermission))
                {
                    break;
                }

                /*
                throw new HttpResponseException()
                    {Code = "FORBIDDEN", StatusCode = 403, Message = "Forbidden."};*/
            }


            request.Headers.Add("userid", validateResponse.UserId.ToString());

            return await base.SendAsync(request, cancellationToken);
        }
    }
}